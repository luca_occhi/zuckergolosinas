﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using System.Data.SqlClient;

namespace Data_Access_Object
{
    public class Empleado_DAO
    {

        public static void Insertar(Empleado empleado)
        {
            //1. Abrir la conexion
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = "Data Source=ARMLGLOCCHIPIQLEXPRESS;Initial Catalog=.PAV2Tarde;Integrated Security=True";
            cn.Open();
            //2. Crear el objeto command para ejecutar el insert
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = cn;
            cmd.CommandText = @"Insert into Empleado (nombre,
                                    apellido,
                                    fecha_nacimiento,
                                    dni,
                                    id_cargo,
                                    puede_realizar_pedidos,
                                    nro_cuenta) values (
                                    @nombre,
                                    @apellido,
                                    @fecha_nacimiento,
                                    @dni,
                                    @id_cargo,
                                    @puede_realizar_pedidos,
                                    @nro_cuenta)";
            cmd.Parameters.AddWithValue("@nombre",empleado.nombre);
            cmd.Parameters.AddWithValue("@apellido", empleado.apellido);
            cmd.Parameters.AddWithValue("@Fecha_nacimiento", empleado.FechaNacimiento);
            cmd.Parameters.AddWithValue("@dni", empleado.dni);
            cmd.Parameters.AddWithValue("@id_cargo", empleado.id_cargo);
            cmd.Parameters.AddWithValue("@puede_realizar_pedidos", empleado.puede_realizar_pedidos);
            cmd.Parameters.AddWithValue("@nro_cuenta", empleado.num_cuenta);
            cmd.ExecuteNonQuery();
            //cmd = new SqlCommand("select Scope_Identity() as ID",cn);
           

            //Cerrar siempre la conexion
            cn.Close();
        } 

        public static void Actualizar(Empleado empleado)
        {
            //1. Abrir la conexion
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = "Data Source=ARMLGLOCCHIPIQLEXPRESS;Initial Catalog=.PAV2Tarde;Integrated Security=True";
            cn.Open();
            //2. Crear el objeto command para ejecutar el insert
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = cn;
            cmd.CommandText = @"update Empleado set apellido = @apellido,
                                    nombre = @nombre,
                                    fecha_nacimiento = @fecha_nacimiento,
                                    dni = @dni,
                                    id_cargo = @id_cargo
                                    puede_realizar_pedidos = @puede_realizar_pedidos,
                                    nro_cuenta = @nro_cuenta
                                    where dni2 = @dni2";
            cmd.Parameters.AddWithValue("@nombre",empleado.nombre);
            cmd.Parameters.AddWithValue("@apellido", empleado.apellido);
            cmd.Parameters.AddWithValue("@Fecha_nacimiento", empleado.FechaNacimiento);
            cmd.Parameters.AddWithValue("@dni", empleado.dni);
            cmd.Parameters.AddWithValue("@id_cargo", empleado.id_cargo);
            cmd.Parameters.AddWithValue("@puede_realizar_pedidos", empleado.puede_realizar_pedidos);
            cmd.Parameters.AddWithValue("@nro_cuenta", empleado.num_cuenta);
            cmd.Parameters.AddWithValue("@dni2", empleado.dni.Value);



            cmd.ExecuteNonQuery();

            //Cerrar siempre la conexion
            cn.Close();
        }

           public static void Eliminar(int dni)
        {
            //1. Abrir la conexion
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = "Data Source=ARMLGLOCCHIPIQLEXPRESS;Initial Catalog=.PAV2Tarde;Integrated Security=True";
            cn.Open();
            //2. Crear el objeto command para ejecutar el insert
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = cn;
            cmd.CommandText = @"delete from Empleado where dni = @dni";
            cmd.Parameters.AddWithValue("@dni", dni);
            cmd.ExecuteNonQuery();

            //Cerrar siempre la conexion
            cn.Close();
        }
    }
}
