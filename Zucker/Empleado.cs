﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Zucker
{
    public class Empleado
    {
        public int id_empleado { get; set; }
        public string nombre { get; set; }
        public string apellido { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public int dni { get; set; }
        public int id_cargo { get; set; }
        public bool puede_realizar_pedidos { get; set; }
        public int num_cuenta { get; set; }
        public override string ToString()
        {
            return string.Format("Numero de empleado : {0} - Apellido: {1} - Nombre: {2} - Fecha de Nacimiento: {3} - DNI: {4} - Cargo: {5} - Puede realizar pedidos: {6} - Numero de cuenta: {7} \n",
                                 id_empleado,
                                 apellido,
                                 nombre,
                                 FechaNacimiento.ToString(FechaNacimiento.Day+"/"+FechaNacimiento.Month+"/"+FechaNacimiento.Year),
                                 dni,
                                 id_cargo,
                                 puede_realizar_pedidos,
                                 num_cuenta);
        }
        public Empleado() { }
        public Empleado(string nom, string ape, DateTime fechaNac, int doc, int id_car, bool pedido, int num_cue )
        {
            nombre = nom;
            apellido = ape;
            FechaNacimiento = fechaNac;
            dni = doc;
            id_cargo = id_car;
            puede_realizar_pedidos = pedido;
            num_cuenta = num_cue;


        }
    }
}